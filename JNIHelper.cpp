#include "AdmobHelper.h"
#include "platform/android/jni/JniHelper.h" //vem do projeto principal - pasta cocos2dx
#include "Definitions.h"
#include <jni.h>
#include "cocos2d.h"

using namespace cocos2d;

extern "C"
{
	void openAppJNI() {
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"openAppJNI"
						,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}
	}

	void rateAppJNI() {
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"rateAppJNI"
						,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}
	}
	void CBShowMoreApps() {
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"CBShowMoreApps"
						,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}
	}

	void showInterstitialJNI() {

    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
				,"showInterstitialJNI"
				,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}

	}

    void showAdmobJNI(){
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"showAdmobJNI"
						,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}
    }
    void hideAdmobJNI(){
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"hideAdmobJNI"
						,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}
    }

    int getBannerHeightJNI(){
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"getBannerHeightJNI"
						,"()I"))
		{
			return t.env->CallStaticIntMethod(t.classID,t.methodID);
		}
		return 0;
    }

    void googlePurchaseRemoveAds() {
    	CCLOG("Raffa: purchase remove ads");
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"purchaseRemoveAds"
						,"()V"))
		{
			t.env->CallStaticVoidMethod(t.classID,t.methodID);
		}
    }

    bool hasRemovedAds() {
    	CCLOG("Raffa: has remove ads");
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"hasRemovedAds"
						,"()Z"))
		{
			return t.env->CallStaticBooleanMethod(t.classID,t.methodID);
		}
		return false;
    }

    bool isChinaMarket() {
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"isChinaMarket"
						,"()Z"))
		{
			return t.env->CallStaticBooleanMethod(t.classID,t.methodID);
		}
		return false;
    }

    JNIEXPORT void JNICALL Java_eu_balzo_aliengarden_AppActivity_nativeOnRemoveAdsPaymentSuccessful() {
        UserDefault::getInstance()->setBoolForKey(REMOVE_ADS_PURCHASED_KEY, true);
        UserDefault::getInstance()->flush();
        cocos2d::EventCustom removeAdsEvent("removedads");
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchEvent(&removeAdsEvent);
    }

    int daysSinceInstall() {
    	JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(t, "eu.balzo.aliengarden/AppActivity"
						,"daysSinceInstall"
						,"()I"))
		{
			return t.env->CallStaticIntMethod(t.classID,t.methodID);
		}
		return 0;
    }
}
